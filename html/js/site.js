function formValidation(){
    var username = document.registration.username;
    var name = document.registration.name;
    var email = document.registration.email;
    var password = document.registration.password;
    var age = document.registration.age;
    
    if(checkUserName(username)){
        if(checkName(name)){
            if(checkPassword(password)){
                if(checkEmail(email)){
                    if(checkAge(age)){
                        return true;
                    }
                }
            }
        }
    }
}

function checkUserName(username){
    if(username.value == null || username.value == ""){
        alert("Username cannot be null!");
        return false;
    }
    return true;
}

function checkName(name){
    if(name.value == null || name.value == ""){
        alert("Name cannot be null!");
        return false;
    }
    return true;
}

function checkPassword(pw){
    var pw_len = pw.value.length;
    if(pw_len == 0){
        alert("Password should not be empty!");
        return false;
    }
    if(pw_len >= 18 || pw_len <= 7){
        alert("Password length should be between 7 and 18!");
        return false;
    }
    return true;
}

function checkEmail(email){
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(email.value.match(mailformat)){
        return true;
    } else {
        alert("Please enter an valid email address!");
        return false;
    }
}

function checkAge(age){
    if(isNaN(age)){
        if(age.value > 0 && age.value <= 99){
            return true;
        } else {
            alert("Please enter an valid age range!");
            return false;
        }
    } else {
        alert("Please enter an integer!");
    }
}